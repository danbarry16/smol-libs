#ifndef PPM
#define PPM "1.0.0"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
enum type{ GRY = 2, COL = 3 };
struct ppm{ enum type t; int z; int w; int h; int* d; };
struct ppm* ppm_new(enum type t, int z, int w, int h){ struct ppm* p =
  (struct ppm*)malloc(sizeof(struct ppm)); p->t = t; p->z = z;
  p->w = w; p->h = h; p->d = (int*)malloc(sizeof(int) * w * h); return p; }
struct ppm* ppm_open(char* n){ char* s = " \t\n\r"; FILE* f = fopen(n, "r");
  char b[256];  int k = -1; fgets(b, 256, f); int t = atoi(strtok(b, s) + 1);
  fgets(b, 256, f); int w = atoi(strtok(b, s)); int h = atoi(strtok(NULL, s));
  fgets(b, 256, f); int z = atoi(strtok(b, s));
  struct ppm* p = ppm_new(t, z, w, h);
  while(fgets(b, 256, f)){ p->d[++k] = atoi(strtok(b, s)); if(p->t == COL)
    p->d[k] = p->d[k] << 16 | atoi(strtok(NULL, s)) << 8 | atoi(strtok(NULL, s));
  } fclose(f); return p; }
void ppm_draw(struct ppm* p, int x, int y, int v){ p->d[(p->w * y) + x] = v; }
void ppm_save(struct ppm* p, char* n){ FILE* f = fopen(n, "w+");
  fprintf(f, "P%i\n%i %i\n%i\n", p->t, p->w, p->h, p->z);
  for(int k = 0; k < p->w * p->h; k++){
    if(p->t == COL) fprintf(f, "%i %i ", p->d[k] >> 16, (p->d[k] >> 8) & 255);
    fprintf(f, "%i\n", p->d[k] & 255); } fclose(f); }
#endif /* Library written by B[], 2022. */
