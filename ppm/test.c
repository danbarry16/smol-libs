#include "ppm.h"

#include <stdbool.h>

void test(int expect, int actual, char* desc){
  bool pass = expect == actual;
  char* res = pass ? "[ OK ]      " : "      [FAIL]";
  printf("%s %s\n", res, desc);
}

int main(){
  char* fn = "temp.ppm";
  struct ppm* p;
  struct ppm* p2;

  /* Draw coloured image */
  p = ppm_new(COL, 256, 3, 3);
  ppm_draw(p, 0, 0, 0x000000);
  ppm_draw(p, 1, 0, 0x000000);
  ppm_draw(p, 2, 0, 0x0000FF);
  ppm_draw(p, 0, 1, 0xFF0000);
  ppm_draw(p, 1, 1, 0x00FF00);
  ppm_draw(p, 2, 1, 0x0000FF);
  ppm_draw(p, 0, 2, 0xFF0000);
  ppm_draw(p, 1, 2, 0x000000);
  ppm_draw(p, 2, 2, 0x0000FF);
  ppm_save(p, fn);

  /* Check the expected pixels exist */
  p2 = ppm_open(fn);
  test(p->t, p2->t, "Colour header type");
  test(p->z, p2->z, "Colour header depth");
  test(p->w, p2->w, "Colour header width");
  test(p->h, p2->h, "Colour header height");
  for(int k = 0; k < p->w * p->h; k++){
    test(p->d[k], p2->d[k], "Colour check pixels");
  }

  /* Free up structure */
  free(p->d);
  free(p);

  /* Draw grey image */
  p = ppm_new(GRY, 256, 3, 3);
  ppm_draw(p, 0, 0, 0x00);
  ppm_draw(p, 1, 0, 0x00);
  ppm_draw(p, 2, 0, 0x88);
  ppm_draw(p, 0, 1, 0x22);
  ppm_draw(p, 1, 1, 0xAA);
  ppm_draw(p, 2, 1, 0x77);
  ppm_draw(p, 0, 2, 0xFF);
  ppm_draw(p, 1, 2, 0x00);
  ppm_draw(p, 2, 2, 0x88);
  ppm_save(p, fn);

  /* Check the expected pixels exist */
  p2 = ppm_open(fn);
  test(p->t, p2->t, "Grey header type");
  test(p->z, p2->z, "Grey header depth");
  test(p->w, p2->w, "Grey header width");
  test(p->h, p2->h, "Grey header height");
  for(int k = 0; k < p->w * p->h; k++){
    test(p->d[k], p2->d[k], "Grey check pixels");
  }

  /* Return nicely */
  return 0;
}
