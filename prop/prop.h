#ifndef PROP
#define PROP "1.0.0"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
struct prop{ char* k; char* v; struct prop* n; };
void prop_del(struct prop* p){ free(p->k); free(p->v); p->k = NULL; }
void prop_set(struct prop* p, char* k, char* v){
  char* _k = (char*)malloc(strlen(k)); strcpy(_k, k);
  char* _v = (char*)malloc(strlen(v)); strcpy(_v, v);
  while(p->n){ if(p->k && !strcmp(p->k, k)) prop_del(p); p = p->n; }
  if(p->k && !strcmp(p->k, k)) prop_del(p);
  p->n = (struct prop*)malloc(sizeof(struct prop));
  p->n->k = _k; p->n->v = _v; p->n->n = NULL; }
struct prop* prop_get(struct prop* p, char* k){ while(p = p->n)
  if(p->k && !strcmp(p->k, k)) return p; return NULL; }
struct prop* prop_open(char* n){ char b[256]; char* v; FILE* f = fopen(n, "r");
  struct prop* p = (struct prop*)malloc(sizeof(struct prop));
  p->k = NULL; p->n = NULL; if(!f) return p;
  while(fgets(b, 256, f)){ v = strtok(b, "="); prop_set(p, b, strtok(NULL, "\n")); }
  fclose(f); return p; }
void prop_save(struct prop* p, char* n){ FILE* f = fopen(n, "w+");
  while(p = p->n) if(p->k && p->v){
    fputs(p->k, f); putc('=', f); fputs(p->v, f); putc('\n', f); } fclose(f); }
#endif /* Library written by B[], 2022. */
