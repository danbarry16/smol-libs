#include "prop.h"

#include <stdbool.h>

void test(char* expect, struct prop* actual, char* desc){
  bool pass = false;
  if(expect == NULL && actual == NULL){
    pass = true;
  }else if(expect == NULL && actual != NULL && actual->v == NULL){
    pass = true;
  }else{
    if(actual != NULL && actual->v != NULL && expect != NULL && strcmp(expect, actual->v) == 0){
      pass = true;
    }
  }
  char* res = pass ? "[ OK ]      " : "      [FAIL]";
  printf("%s %s\n", res, desc);
}

int main(){
  char* fn = "temp.txt";
  struct prop* p = prop_open(fn);
  struct prop* t = NULL;

  test("world", prop_get(p, "hello"), "Retrieve existing value from file");

  test(NULL, prop_get(p, "fake"), "Retrieve non-existing entry from file");

  prop_set(p, "hello", "new");
  test("new", prop_get(p, "hello"), "Overwrite an existing value");

  prop_del(prop_get(p, "hello"));
  test(NULL, prop_get(p, "hello"), "Delete an existing value");

  prop_set(p, "hi", "there");
  test("there", prop_get(p, "hi"), "Set a new value and retrieve it");

  prop_set(p, "where", "you?");
  test("you?", prop_get(p, "where"), "Set a second value and retrieve it");

  prop_save(p, fn);
  p = prop_open(fn);
  test("there", prop_get(p, "hi"), "Load first value from saved file");
  test("you?", prop_get(p, "where"), "Load second value from saved file");

  /* Return nicely */
  return 0;
}
