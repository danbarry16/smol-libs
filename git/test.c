#include "git.h"

void print_and_free(char** r){
  int x;
  for(x = 0; r[x][0] > 0 && x < N; x++){
    printf("%s", r[x]);
    free(r[x]);
  }
  free(r[x]);
  free(r);
}

int main(){
  printf("Git location:  %s\n", G);
  printf("Git version:   %s\n", GIT);
  printf("Buffer length: %i\n", N);

  git g = { .d = "../", .r = "origin", .b = "master" };
  char** r;

  printf("\n# Log\n");
  r = g_log(&g, 0);
  print_and_free(r);
  r = g_log(&g, 1);
  print_and_free(r);

  printf("\n# Log Commit\n");
  r = g_logc(&g, "4231b39");
  print_and_free(r);

  /* TODO: Add. */
  /* TODO: Commit. */

  printf("\n# Tree\n");
  r = g_tree(&g);
  print_and_free(r);

  printf("\n# Checkout\n");
  r = g_check(&g, "master");
  print_and_free(r);

  printf("\n# Branch\n");
  r = g_branch(&g);
  print_and_free(r);

  printf("\n# Tag\n");
  r = g_tag(&g);
  print_and_free(r);

  printf("\n# Status\n");
  r = g_status(&g);
  print_and_free(r);

  printf("\n# Fetch\n");
  r = g_fetch(&g);
  print_and_free(r);

  /* TODO: Pull. */
  /* TODO: Push. */

  /* Return nicely */
  return 0;
}
