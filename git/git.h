#pragma once        /* git.h - Lightweight wrapper around the terminal  */
#include <stdio.h>  /*         git client written in C.                 */
#include <stdlib.h> /* Written by B[], 2022                             */
typedef struct{ char *d, *r, *b; } git; char* G = "/usr/bin/git"; int N = 256;
char** g_exec(char* d, char* a){ char x[N], **b = (char**)malloc(N * sizeof(*b));
  sprintf(x, "cd %s && %s %s", d, G, a); FILE* f = popen(x, "r"); int i = 0;
  while(i < N) b[i++] = (char*)malloc(N * sizeof(*b)); i = -1; while(b[++i] && i < N - 1)
  fgets(b[i], N, f); b[i][0] = 0; pclose(f); return b; } char* GIT = "0.3.2";
#define g_x_1(g, s, a) ({ char x[N]; sprintf(x, s, a); g_exec(g->d, x); })
#define g_x_2(g, s, a, b) ({ char x[N]; sprintf(x, s, a, b); g_exec(g->d, x); })
#define g_x_3(g, s, a, b, c) ({ char x[N]; sprintf(x, s, a, b, c); g_exec(g->d, x); })
char** g_logn(git* g, int s, int n){ return g_x_3(g, "log %s --oneline -n %i --skip=%i", g->b, n, s); }
char** g_log(git* g, int s){ return g_logn(g, s, 1); }
char** g_logc(git* g, char* c){ return g_x_1(g, "log %s --format=full -n 1", c); }
char** g_add(git* g, char* f){ return g_x_1(g, "add %s", f); }
char** g_commit(git* g, char* m){ return g_x_1(g, "commit -m %s", m); }
char** g_tree(git* g){ return g_x_1(g, "ls-tree -t %s --full-name", g->b); }
char** g_check(git* g, char* c){ return g_x_1(g, "checkout %s", c); }
char** g_branch(git* g){ return g_exec(g->d, "branch"); }
char** g_tag(git* g){ return g_exec(g->d, "tag"); }
char** g_status(git* g){ return g_exec(g->d, "status -s"); }
char** g_fetch(git* g){ return g_x_1(g, "pull %s", g->r); }
char** g_pull(git* g){ return g_x_2(g, "pull %s %s", g->r, g->b); }
char** g_push(git* g){ return g_x_2(g, "push %s %s", g->r, g->b); }
void g_free(char** r){ for(int x = 0; x < N; x++) free(r[x]); free(r); }
