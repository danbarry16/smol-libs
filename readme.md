# Smol Libs

A series of small C libraries contained within source code of 80x25 characters.

![xgui demo output](doc/calc.png)

## Libraries

* [git](git/) - Library that allows reading git repositories and performing
basic actions on them.
* [json](json/) - Parse JSON files from strings and disk, dumping changes as a
string.
* [ppm](ppm/) - Read and write
[PPM](http://davis.lbl.gov/Manuals/NETPBM/doc/ppm.html) and
[PGM](http://davis.lbl.gov/Manuals/NETPBM/doc/pgm.html) files. Doesn't
support comments in files.
* [prop](prop/) - Read and write property files. Similar to Bash variables and
Java Properties files.
* [serv](serv/) - Basic multi-threaded HTTP web server that parses headers.
* [xgui](xgui/) - A simple X11 window that offers strings (with fonts),
buttons, images and input events.

## Future

* Hash Map - A simple hash map implementation.
* Logging - Multi-threaded logging.
* SVG - Write SVG files to disk.
* Testing - Compile time unit testing.
* TUI - Simple text user interface.
* WAV - Read and write WAV files.
