#include "serv.h"

#include <stdio.h>

#define PORT 8080

void process(char** r, char** q){
  char* buff = (char*)malloc(L * sizeof(char));
  char* req = r[H("request") % L];
  char* loc = r[H("location") % L];
  q[H("response") % L] = "HTTP/1.1 200 OK";
  if(strcmp(req, "GET") == 0){
    if(strcmp(loc, "/"     ) == 0 ||
       strcmp(loc, "/index") == 0){
      sprintf(buff,
        "<h1>Hello!</h1>"
        "<br>Server version: %s"
        "<br>User agent: %s"
        "<br><form action=\"/\" method=\"post\">"
          "<br>Username: <input type=\"text\" id=\"user\" name=\"user\">"
          "<br>Password: <input type=\"password\" id=\"pass\" name=\"pass\">"
          "<br><input type=\"submit\" value=\"Submit\">"
        "</form>",
        SERV,
        r[H("User-Agent:") % L]
      );
    }else sprintf(buff, "<h1>Error</h1>Unknown location '%s'", loc);
  }else if(strcmp(req, "POST") == 0){
    sprintf(buff,
      "<h1>POST Received</h1>"
      "<br>Username is: %s"
      "<br>Password is: %s",
      r[H("user") % L],
      r[H("pass") % L]
    );
  }else sprintf(buff, "<h1>Error</h1>Unknown request '%s'", req);
  q[1] = "Server: serv";
  q[2] = "Set-Cookie: omnomnom";
  q[H("payload") % L] = buff;
}

int main(){
  /* Initialise the server */
  serv s = {};
  serv_init(&s, PORT);

  /* Pick-up some clients */
  int pid = getpid();
  while(pid == getpid()) serv_update(&s, process);

  /* Return nicely */
  return 0;
}
