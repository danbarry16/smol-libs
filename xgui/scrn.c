#include "xgui.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAP_WIDTH 80
#define MAP_HEIGHT 60

int running;
xgui x;
xobj* map;

typedef enum{ NORTH = 0, EAST = 1, SOUTH = 2, WEST = 3 } dir;
struct AGENT{ int x, y; dir d; } agent;

void callback(XEvent e, xobj* o, char c, float mx, float my, int w, int h){
  /* Check if we need to bail */
  if(o->i == 0 && (e.type == KeyPress || e.type == ButtonPress)){
    running = 0;
  }
  /* Only one update per callback set */
  if(o->i == 0){
    int z = agent.x + (agent.y * MAP_WIDTH);
    /* Trun agent based on patch colour */
    if(map[z].t == TXT) agent.d = (agent.d + 1) % 4;
    else agent.d = (agent.d - 1) % 4;
    /* Move forward a patch */
    switch(agent.d){
      case NORTH : agent.y--; break;
      case EAST  : agent.x++; break;
      case SOUTH : agent.y++; break;
      case WEST  : agent.x--; break;
    }
    /* Loop agent if outside */
    if(agent.x < 0) agent.x = MAP_WIDTH - 1;
    if(agent.x >= MAP_WIDTH) agent.x = 0;
    if(agent.y < 0) agent.y = MAP_HEIGHT - 1;
    if(agent.y >= MAP_HEIGHT) agent.y = 0;
    /* Flip colour of current patch. */
    z = agent.x + (agent.y * MAP_WIDTH);
    map[z].t = map[z].t == TXT ? BTN : TXT;
  }
}

int main(){
  /* Setup the GUI */
  running = 1;
  xgui_init(&x, 400, 300, 0xFFFFFFFF, 0x00000000);

  /* Setup map */
  float w = 1.0 / MAP_WIDTH;
  float h = 1.0 / MAP_HEIGHT;
  map = (xobj*)malloc(MAP_WIDTH * MAP_HEIGHT * sizeof(xobj));
  for(int j = 0; j < MAP_HEIGHT; j++){
    for(int i = 0; i < MAP_WIDTH; i++){
      int z = i + (j * MAP_WIDTH);
      map[z].i = z;
      map[z].t = TXT;
      map[z].x = (i + 0.5) * w;
      map[z].y = (j + 0.5) * h;
      map[z].w = w;
      map[z].h = h;
      map[z].v = " ";
      xgui_add(&x, &map[z]);
    }
  }

  /* Setup agent */
  agent.x = MAP_WIDTH / 2;
  agent.y = MAP_HEIGHT / 2;
  agent.d = NORTH;

  /* Keep updating the GUI */
  while(running){
    xgui_update(&x, callback);
    XEvent e;
    XSendEvent(x.d, x.w, True, ExposureMask, &e); // Force repaint
    XSync(x.d, False);
    usleep(100000); // Space out the time between updates
  }

  /* Free up resources */
  free(map);

  /* Return nicely */
  return 0;
}
