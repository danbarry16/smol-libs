#include "xgui.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

enum OP{ ADD = 13, SUB = 12, DIV = 10, MUL = 11, NONE = -1 } op;
enum CMD{ EQL = 20, CLR = 21 };
enum RES{ OK = 0, DIV_ZERO = 1 } res;

struct NUM{ bool set; bool neg; long long val; } a, b;

xgui x;
xobj corn;
char display[256];

void calc_reset(){
  a.set = false;
  a.neg = false;
  a.val = 0;
  op = NONE;
  b.set = false;
  b.neg = false;
  b.val = 0;
  res = OK;
}

void calc_eval(){
  long long n = 0;
  if(a.set){
    n = a.neg ? -a.val : a.val;
    if(op != NONE && b.set){
      long long t = b.neg ? -b.val : b.val;
      switch(op){
        case SUB :
          n -= t;
          break;
        case ADD :
          n += t;
          break;
        case DIV :
          if(t != 0) n /= t;
          else{
            calc_reset();
            res = DIV_ZERO;
            return;
          }
          break;
        case MUL :
          n *= t;
          break;
      }
    }
  }
  calc_reset();
  a.set = true;
  a.neg = n < 0;
  a.val = n < 0 ? -n : n;
  res = OK;
}

void callback(XEvent e, xobj* o, char c, float mx, float my, int w, int h){
  char i = -1;
  /* Get input */
  if(o->i == 0 && e.type == KeyPress){
    if(c >= '0' && c <= '9') i = c - '0';
    else if(c == '-'             ) i = SUB;
    else if(c == '+'             ) i = ADD;
    else if(c == '/'             ) i = DIV;
    else if(c == '*' || c == '*' ) i = MUL;
    else if(c == '=' || c == '\r') i = EQL;
    else if(c == 'c' || c == 27  ) i = CLR;
  }
  if(e.type == ButtonPress){
    if(mx >= o->x - o->w / 2 && mx <= o->x + o->w / 2 &&
       my >= o->y - o->h / 2 && my <= o->y + o->h / 2)
      i = o->i;
  }
  /* If we got interesting input, do something */
  if(i >= 0){
    switch(i){
      case 0 :
      case 1 :
      case 2 :
      case 3 :
      case 4 :
      case 5 :
      case 6 :
      case 7 :
      case 8 :
      case 9 :
        if(op == NONE){
          a.set = true;
          a.val *= 10;
          a.val += i;
        }else{
          b.set = true;
          b.val *= 10;
          b.val += i;
        }
        break;
      case SUB :
        if(op == NONE && !a.set){
          a.neg = true;
          break;
        }
        if(op != NONE && !b.set){
          b.neg = true;
          break;
        }
      case ADD :
      case DIV :
      case MUL :
        if(op != NONE) calc_eval();
        op = i;
        break;
      case EQL :
        calc_eval();
        break;
      case CLR :
        calc_reset();
        break;
      default :
        printf("Unsupported %i\n", i);
        break;
    }
  }
  /* Scale the font based on the window size */
       if(w < 300 || h < 300) xf = "-*-helvetica-*-r-*-*-12-*-*-*-*-*-*-*";
  else if(w < 600 || h < 600) xf = "-*-helvetica-*-r-*-*-16-*-*-*-*-*-*-*";
  else if(w < 900 || h < 900) xf = "-*-helvetica-*-r-*-*-24-*-*-*-*-*-*-*";
  else                        xf = "-*-helvetica-*-r-*-*-32-*-*-*-*-*-*-*";
  /* Put diagonal image in the diagonal */
  if(o->i == -10){
    /* By default, draw off-screen */
    o->x = 1;
    o->y = 1;
    /* If there is space, draw in the bottom right corner */
    if(w > corn.a) o->x = (float)(w - o->a) / w;
    if(h > corn.b) o->y = (float)(h - o->b) / h;
  }
  /* Finally, update the display */
  if(res == OK){
    long long num = 0;
    if(b.set) num = b.neg ? -b.val : b.val;
    else if(a.set) num = a.neg ? -a.val : a.val;
    sprintf(display, "%lli", num);
  }else if(res == DIV_ZERO) sprintf(display, "DIV ZERO ERR");
  else sprintf(display, "UNKNOWN ERR");
}

int main(){
  /* Setup GUI objects */
  float w = 0.2;
  float ow = w * 1.1;
  float h = 0.175;
  float oh = h * 1.1;
  xobj  s = { .i =  -1, .t = TXT, .x = 0.5,     .y = 0.1,       .w = 0.8, .h = 0.1, .v = display };
  xobj b7 = { .i =   7, .t = BTN, .x = 0.15,    .y = s.y+s.h*2, .w = w,   .h = h,   .v = "7"     };
  xobj b8 = { .i =   8, .t = BTN, .x = b7.x+ow, .y = s.y+s.h*2, .w = w,   .h = h,   .v = "8"     };
  xobj b9 = { .i =   9, .t = BTN, .x = b8.x+ow, .y = s.y+s.h*2, .w = w,   .h = h,   .v = "9"     };
  xobj bd = { .i = DIV, .t = BTN, .x = b9.x+ow, .y = s.y+s.h*2, .w = w,   .h = h,   .v = "/"     };
  xobj b4 = { .i =   4, .t = BTN, .x = b7.x,    .y = b7.y+oh,   .w = w,   .h = h,   .v = "4"     };
  xobj b5 = { .i =   5, .t = BTN, .x = b8.x,    .y = b8.y+oh,   .w = w,   .h = h,   .v = "5"     };
  xobj b6 = { .i =   6, .t = BTN, .x = b9.x,    .y = b9.y+oh,   .w = w,   .h = h,   .v = "6"     };
  xobj bm = { .i = MUL, .t = BTN, .x = bd.x,    .y = bd.y+oh,   .w = w,   .h = h,   .v = "x"     };
  xobj b1 = { .i =   1, .t = BTN, .x = b4.x,    .y = b4.y+oh,   .w = w,   .h = h,   .v = "1"     };
  xobj b2 = { .i =   2, .t = BTN, .x = b5.x,    .y = b5.y+oh,   .w = w,   .h = h,   .v = "2"     };
  xobj b3 = { .i =   3, .t = BTN, .x = b6.x,    .y = b6.y+oh,   .w = w,   .h = h,   .v = "3"     };
  xobj bs = { .i = SUB, .t = BTN, .x = bm.x,    .y = bm.y+oh,   .w = w,   .h = h,   .v = "-"     };
  xobj b0 = { .i =   0, .t = BTN, .x = b1.x,    .y = b1.y+oh,   .w = w,   .h = h,   .v = "0"     };
  xobj bc = { .i = CLR, .t = BTN, .x = b2.x,    .y = b2.y+oh,   .w = w,   .h = h,   .v = "C"     };
  xobj be = { .i = EQL, .t = BTN, .x = b3.x,    .y = b3.y+oh,   .w = w,   .h = h,   .v = "="     };
  xobj ba = { .i = ADD, .t = BTN, .x = bs.x,    .y = bs.y+oh,   .w = w,   .h = h,   .v = "+"     };

  /* Setup the GUI */
  xgui_init(&x, 300, 300, 0xFFFFFFFF, 0x00000000);

  /* Setup the background images */
  corn.i = -11;
  corn.t = IMG;
  corn.x = 0;
  corn.y = 0;
  xgui_image(&x, "corner.xbm", &(corn.d), &(corn.a), &(corn.b));
  xgui_add(&x, &corn); // NOTE: Is background, so is first.
  xobj peng = { .i = -10, .t = IMG, .x = 1, .y = 1 };
  xgui_image(&x, "penguin.xbm", &(peng.d), &(peng.a), &(peng.b));
  xgui_add(&x, &peng); // NOTE: Is background, so is second.

  /* Add object to GUI */
  xgui_add(&x, &b7); xgui_add(&x, &b8); xgui_add(&x, &b9); xgui_add(&x, &bd);
  xgui_add(&x, &b4); xgui_add(&x, &b5); xgui_add(&x, &b6); xgui_add(&x, &bm);
  xgui_add(&x, &b1); xgui_add(&x, &b2); xgui_add(&x, &b3); xgui_add(&x, &bs);
  xgui_add(&x, &b0); xgui_add(&x, &bc); xgui_add(&x, &be); xgui_add(&x, &ba);
  xgui_add(&x, &s); // NOTE: This is rendered last on purpose.

  /* Go in with a clean state */
  calc_reset();

  /* Keep updating the GUI */
  for(;;){
    xgui_update(&x, callback);
    usleep(1000); // Give the CPU a break (1ms is unnoticeable to users)
  }

  /* Return nicely */
  return 0;
}
