#pragma once          /* xgui - A light wrapper around Xlib that offers */
#include <X11/Xlib.h> /*        text, buttons and images.               */
#include <string.h>   /* Written by B[], 2022                           */
enum type{ TXT, BTN, IMG }; char* xf = "-*-helvetica-*-r-*-*-16-*-*-*-*-*-*-*";
typedef struct{ int i, a, b; enum type t; float x, y, w, h; char* v; Pixmap d; } xobj;
typedef struct{ Display* d; int s, n; Window w; GC g; long b, f; xobj* o[65536]; } xgui;
void xgui_init(xgui* x, int w, int h, long b, long f){ x->b = b; x->f = f;
  x->d = XOpenDisplay(NULL); x->s = DefaultScreen(x->d); x->n = 0; x->g = DefaultGC(x->d, x->s);
  x->w = XCreateSimpleWindow(x->d, RootWindow(x->d, x->s), 0, 0, w, h, 1, x->f, x->b);
  XSelectInput(x->d, x->w, ExposureMask|KeyPressMask|ButtonPressMask); XMapWindow(x->d, x->w); }
void xgui_add(xgui* x, xobj* o){ if(x->n + 1 < 65536) x->o[x->n++] = o; }
void xgui_update(xgui* x, void (*c)(XEvent, xobj*, char, float, float, int, int)){
  XEvent e; XNextEvent(x->d, &e); XWindowAttributes a; XGetWindowAttributes(x->d, x->w, &a);
  float w = a.width, h = a.height; int l, eks, ekc, i = -1; xobj o;
  XFontStruct* f = XLoadQueryFont(x->d, xf); XSetFont(x->d, x->g, f->fid);
  while(++i < x->n){ o = *(x->o[i]); eks = e.xkey.state; ekc = e.xkey.keycode; if(o.t != IMG){
    XSetForeground(x->d, x->g, o.t == BTN ? x->f : x->b); eks &= ShiftMask; l = strlen(o.v);
    XFillRectangle(x->d, x->w, x->g, w * (o.x - o.w/2), h * (o.y - o.h/2), w*o.w, h*o.h);
    XSetForeground(x->d, x->g, o.t == BTN ? x->b : x->f); XDrawString(x->d,
      x->w, x->g, (w * o.x) - (XTextWidth(f, o.v, l)/2), h*o.y, o.v, l);
  }else if(e.type == Expose) XCopyPlane(x->d, o.d, x->w, x->g, 0, 0, o.a, o.b, o.x*w, o.y*h, 1);
  (*c)(e, x->o[i], XKeycodeToKeysym(x->d, ekc, eks ? 1 : 0), e.xbutton.x/w, e.xbutton.y/h, w, h);
}} void xgui_close(xgui* x){ XDestroyWindow(x->d, x->w); XCloseDisplay(x->d); }
int xgui_image(xgui* x, char* f, Pixmap* p, int* a, int* b){ int i, j; return
  XReadBitmapFile(x->d, x->w, f, a, b, p, &i, &j); } char* XGUI = "1.1.0";
