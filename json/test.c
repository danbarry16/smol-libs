#include "json.h"

#include <stdbool.h>

int pass = 0, total = 0;

void test(bool res, char* grp, char* desc){
  if(res) ++pass;
  char* out = res ? "[ OK ]      " : "      [FAIL]";
  printf("[%i\t] %s %s -> %s\n", total++, out, grp, desc);
}

int end(){
  printf("--------------------------------\n");
  printf("%s ( %i / %i )\n", pass == total ? "SUCCESS" : "FAILURE", pass, total);
  printf("--------------------------------\n");
  return pass == total ? 0 : 1;
}

int main(){
  json* j;
  char* g;
  char* s;
  char* d;

  /* String concat */
  char* a = malloc(256);
  sprintf(a, "%s", "Hey there, Delilah, ");
  char* b = "what's it like in New York City?";
  cc(&a, b);
  test(strcmp(a, "Hey there, Delilah, what's it like in New York City?") == 0, "String", "Concat");
  free(a);

  /* String clone */
  sprintf(a, "%s", "Oh, it's what you do to me");
  b = cs(a);
  test(strcmp(b, "Oh, it's what you do to me") == 0, "String", "Clone");
  free(b);

  /* Empty */
  g = "Empty";
  char* t0 = "{}";
  s = cs(t0);
  j = json_parse(s);
  free(s);
  test(j->k == NULL,       g, "Key NULL");
  test(j->v == NULL,       g, "Value NULL");
  test(j->c != NULL,       g, "Children not NULL");
  test(j->n == 1,          g, "Children count one");
  test(j->c[0].k == NULL,  g, "Child key NULL");
  test(j->c[0].v == NULL,  g, "Child value NULL");
  test(j->c[0].c == NULL,  g, "Child children NULL");
  test(j->c[0].n == 0,     g, "Child children count zero");
  d = json_dump(j);
  test(strcmp(d, t0) == 0, g, "Dump string");
  free(d);
  json_free(j);

  /* Single child */
  g = "Single";
  char* t1 = "{\"hello\":\"world\"}";
  s = cs(t1);
  j = json_parse(s);
  free(s);
  test(j->k == NULL,                    g, "Key NULL");
  test(j->v == NULL,                    g, "Value NULL");
  test(j->c != NULL,                    g, "Children not NULL");
  test(j->n == 1,                       g, "Children count one");
  test(j->c[0].k != NULL,               g, "Child key not NULL");
  test(strcmp(j->c[0].k, "hello") == 0, g, "Child key is 'hello'");
  test(j->c[0].v != NULL,               g, "Child value not NULL");
  test(strcmp(j->c[0].v, "world") == 0, g, "Child value is 'world'");
  test(j->c[0].c == NULL,               g, "Child has no children");
  test(j->c[0].n == 0,                  g, "Child count zero");
  d = json_dump(j);
  test(strcmp(d, t1) == 0,              g, "Dump string");
  free(d);
  json_free(j);

  /* Multiple children */
  g = "Multi";
  char* t2 = "{{\"hello\":\"world\"},{\"boop\":\"beep\"}}";
  s = cs(t2);
  j = json_parse(s);
  free(s);
  test(j->n == 1,                            g, "Children count one");
  test(j->c[0].n == 2,                       g, "Child children count two");
  test(j->c[0].c != NULL,                    g, "Child children not NULL");
  test(j->c[0].c[0].k != NULL,               g, "Child children key not NULL");
  test(strcmp(j->c[0].c[0].k, "hello") == 0, g, "Child children key is 'hello'");
  test(       j->c[0].c[0].v != NULL,        g, "Child children value not NULL");
  test(strcmp(j->c[0].c[0].v, "world") == 0, g, "Child children value is 'world'");
  test(       j->c[0].c[0].c == NULL,        g, "Child children has no children");
  test(       j->c[0].c[0].n == 0,           g, "Child children count zero");
  test(       j->c[0].c[1].k != NULL,        g, "Child children key not NULL");
  test(strcmp(j->c[0].c[1].k, "boop") == 0,  g, "Child children key is 'boop'");
  test(       j->c[0].c[1].v != NULL,        g, "Child children value not NULL");
  test(strcmp(j->c[0].c[1].v, "beep") == 0,  g, "Child children value is 'beep'");
  test(       j->c[0].c[1].c == NULL,        g, "Child children has no children");
  test(       j->c[0].c[1].n == 0,           g, "Child children count zero");
  d = json_dump(j);
  test(strcmp(d, t2) == 0,                   g, "Dump string");
  free(d);
  json_free(j);

  /* Russian dolls */
  g = "Russian";
  char* t3 =
"{\
  { \"a\": \"b\" },\
  {\
    { \"c\": \"d\" },\
    [\
      { \"e\": \"f\" },\
      { \"g\": \"h\" }\
    ]\
  }\
}";
  s = cs(t3);
  j = json_parse(s);
  free(s);
  test(j->n == 1,                                  g, "Root children");
  test(j->c[0].n == 2,                             g, "Two children");
  test(j->c[0].c[0].n == 0,                        g, "No children");
  test(j->c[0].c[1].n == 2,                        g, "Two children");
  test(j->c[0].c[1].c[0].n == 0,                   g, "No children");
  test(j->c[0].c[1].c[1].n == 2,                   g, "Two children");
  test(j->c[0].c[1].c[1].c[0].n == 0,              g, "No children");
  test(j->c[0].c[1].c[1].c[1].n == 0,              g, "No children");
  test(strcmp(j->c[0].c[0].k, "a") == 0,           g, "Key check");
  test(strcmp(j->c[0].c[0].v, "b") == 0,           g, "Value check");
  test(strcmp(j->c[0].c[1].c[0].k, "c") == 0,      g, "Key check");
  test(strcmp(j->c[0].c[1].c[0].v, "d") == 0,      g, "Value check");
  test(strcmp(j->c[0].c[1].c[1].c[0].k, "e") == 0, g, "Key check");
  test(strcmp(j->c[0].c[1].c[1].c[0].v, "f") == 0, g, "Value check");
  test(strcmp(j->c[0].c[1].c[1].c[1].k, "g") == 0, g, "Key check");
  test(strcmp(j->c[0].c[1].c[1].c[1].v, "h") == 0, g, "Value check");

  /* Add object */
  g = "Add";
  char* t4 = "{}";
  s = cs(t4);
  j = json_parse(s);
  free(s);
  test(j->n == 1,          g, "Children count one");
  test(j->c[0].n == 0,     g, "Children count zero");
  json* z = (json*)malloc(sizeof(json));
  z->k = cs("a");
  z->v = cs("b");
  z->c = NULL;
  z->n = 0;
  json_add(&(j->c[0]), z);
  test(j->n == 1,          g, "Children count one");
  test(j->c[0].n == 1,     g, "Children count one");
  d = json_dump(j);
  test(strcmp(d, "{{\"a\":\"b\"}}") == 0, g, "Dump string");
  free(d);
  json_free(j);

  /* Read file */
  g = "File";
  j = json_open("test.json");
  d = json_dump(j);
  test(strcmp(d, "{{\"a\":\"b\"},{\"c\":{{\"d\"},{\"e\"}}}}") == 0, g, "Check file");
  free(d);
  json_free(j);

  /* Return nicely */
  return end();
}
