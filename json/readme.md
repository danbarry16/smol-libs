# JSON

This is a small JSON parser in C, written to be as small as possible, with the
following capabilities:

* *Parse JSON string* - We can parse a carefully structured JSON string (see
below).
* *Read JSON from file* - We can read a text file from disk and then parse it.
* *Add JSON objects* - We can dynamically add or change JSON objects in their
tree structure.
* *Dump JSON structure as string* - We can dump this structure as a string.
* *Recursively free JSON structure* - We can then free up associated memory for
the JSON tree structure.

As the parser is so small, it also has the following limitations:

* *No parse checking* - It is your responsibility to parse well formed JSON.
* *No file writing* - We can dump the JSON string using `json_dump()`, but ran
out of space for `json_save()`. This shouldn't be that complicated to
implement though.
* *Nothing but strings* - We cannot parse anything but strings, but this isn't
so bad anyway.
* *No string escaping* - Any time we see `"` we assume this is the start or end
of the string, irrespective of escaping. If you cannot control your input
strings, consider encoding them, for example with Base64 (more efficient
encodings available).
* *All objects must be wrapped* - All objects mus be wrapped, for example:

```
{ "a": "b", "c": "d" }         // Not valid in this parser
{ { "a": "b" }, { "c": "d" } } // Valid in this parser
```

* *Structures can't have labels* - Objects cannot be directly labelled and must
exist in a container:

```
{ "a": { "b": "c" }, "d": { "e": "f" } }         // Not valid in this parser
{ { "a": { "b": "c" } }, { "d": { "e": "f" } } } // Valid in this parser
```

**Lastly:** You are insane to use this in anything important, but if you do,
please let me know so we can all laugh about it together.

## Examples

The following are examples showing how to use the JSON library.

### Parse From File

```c
json* j = json_open("test.json"); // Open the JSON file
char* d = json_dump(j);           // Convert the parsed JSON to a string
printf("%s\n", d);                // Display the string
free(d);                          // Free the allocated string
json_dump(j);                     // Free root JSON object recursively
```

### Parse From String

```c
char* t = "{\"a\":\"b\"}";        // The raw JSON string
char* s = cs(t);                  // Take a clone of the string
json* j = json_parse(s);          // Parse the cloned string
free(s);                          // Free the string clone
char* d = json_dump(j);           // Convert the parsed JSON to a string
printf("%s\n", d);                // Display the string
free(d);                          // Free the allocated string
json_dump(j);                     // Free root JSON object recursively
```

### Get String (by offset)

```c
char* t = "{\"a\":\"b\"}";        // The raw JSON string
char* s = cs(t);                  // Take a clone of the string
json* j = json_parse(s);          // Parse the cloned string
free(s);                          // Free the string clone

json* o = j->c[0];                // Get the desired object

d = json_dump(o);                 // Get string representation of object
printf("%s\n", d);                // Print the desired object
free(d);                          // Free the allocated string
json_dump(j);                     // Free root JSON object recursively
```

### Get String (by name)

```c
char* t = "{\"a\":\"b\"}";        // The raw JSON string
char* s = cs(t);                  // Take a clone of the string
json* j = json_parse(s);          // Parse the cloned string
free(s);                          // Free the string clone

char* k = "a";                    // The key to be found
json* o = NULL;                   // No object located
for(int x = 0; x < j->n; x++)     // Loop over children
  if(strcmp(j->c[x].k, k) == 0){  // If we find the object, store it
    o = j->c[x];
    break;                        // Stop searching
  }

if(o != NULL){
  d = json_dump(o);               // Get string representation of object
  printf("%s\n", d);              // Print the desired object
}
free(d);                          // Free the allocated string
json_dump(j);                     // Free root JSON object recursively
```

### Get Integer

```c
char* t = "{\"a\":\"69\"}";       // The raw JSON string
char* s = cs(t);                  // Take a clone of the string
json* j = json_parse(s);          // Parse the cloned string
free(s);                          // Free the string clone

int i = atoi(j->c[0].v);          // Get the number

printf("%i\n", i);                // Print the desired object
free(d);                          // Free the allocated string
json_dump(j);                     // Free root JSON object recursively
```

### Add JSON Object

```c
char* t = "{}";        // The raw JSON string
char* s = cs(t);                  // Take a clone of the string
json* j = json_parse(s);          // Parse the cloned string
free(s);                          // Free the string clone

json* z = (json*)malloc(sizeof(json)); // Allocate new child
z->k = cs("a");                   // Set key
z->v = cs("b");                   // Set value
z->c = NULL;                      // Has no children
z->n = 0;                         // Children count zero
json_add(&(j->c[0]), z);          // Finally, add the child

d = json_dump(j);                 // Get string representation of object
printf("%s\n", d);                // Print the desired object
free(d);                          // Free the allocated string
json_dump(j);                     // Free root JSON object recursively
```

### Write To File

```c
char* t = "{\"a\":\"b\"}";        // The raw JSON string
char* s = cs(t);                  // Take a clone of the string
json* j = json_parse(s);          // Parse the cloned string
free(s);                          // Free the string clone

FILE* f = fopen("a.json", "w+");  // Open the file for writing
char* d = json_dump(j);           // Convert the parsed JSON to a string
fputs(d, f);                      // Write the dump to file
fclose(f);                        // Close the file

free(d);                          // Free the allocated string
json_dump(j);                     // Free root JSON object recursively
```

## FAQ

1. Q: *Why would I use this?* A: You wouldn't.
2. Q: *I found a bug...* A: Most likely, this is not a compliant parser. Most
bugs are now features these days anyway.
3. Q: *I want to add feature X...* A: Feel free to fork and make these changes.
Obviously these will only be merged if it meets the strict coding restrictions.
4. Q: *Why is this readme larger than the code?* A: Because the code is dense,
but people are denser.
